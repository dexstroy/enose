package com.example.myfirstapp.sharedPrefs;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.example.myfirstapp.MainMenu;

public class PrefsManager{

    private SharedPreferences sharedPreferences;

    private String prefName = "MyPrefs";

    public PrefsManager(Context context){
        sharedPreferences = context.getSharedPreferences(prefName, Context.MODE_PRIVATE);
    }

    // MQ3 data
    public void saveMQ3_R0_value(int value){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("MQ3air", value);
        editor.commit();
        Log.d("LOG", "MQ3air value saved");
    }

    public int getMQ3_R0_value(){
        return sharedPreferences.getInt("MQ3air", 70);

    }

    // MQ135 data
    public void saveMQ135_R0_value(int value){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("MQ135air", value);
        editor.commit();
        Log.d("LOG", "MQ135air value saved");
    }

    public int getMQ135_R0_value(){
        return sharedPreferences.getInt("MQ135air", 30);

    }

    public void saveSampleRate(int sampleRate){
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("sampleRate", sampleRate);
        editor.commit();
    }

    public int getSampleRate(){
        return sharedPreferences.getInt("sampleRate", 1000);

    }
}
