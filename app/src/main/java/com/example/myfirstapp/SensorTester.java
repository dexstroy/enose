package com.example.myfirstapp;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myfirstapp.dataLog.DataLogger;
import com.example.myfirstapp.dataLog.DataManager;
import com.example.myfirstapp.graph.SensorGraphCard;
import com.example.myfirstapp.sharedPrefs.PrefsManager;
import com.github.mikephil.charting.charts.LineChart;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

public class SensorTester extends Activity {

    SensorValueUpdater valueUpdater;
    FloatingActionButton fab;

    CalculatorPPM calculatorPPM;

    DecimalFormat df = new DecimalFormat("#.####");

    DataLogger dataLogger;

    long startTime;


    TimeZone tz = TimeZone.getTimeZone("UTC");
    SimpleDateFormat timeFormat;
    TextView timeText;
    SensorGraphCard[] graphs;
    TextView[] textDisplays;

    PrefsManager prefsManager;
    int sampleRate;


    // Custom save dialog
    class SaveDialog extends Dialog{

        public Activity c;
        public Dialog d;
        Spinner spinner;
        DataManager dataManager;
        Button cancelButton, recordButton;
        EditText meritevName;
        SensorTester sensorTester;

        public SaveDialog(Activity a, SensorTester sensorTester) {
            super(a);
            // TODO Auto-generated constructor stub
            this.c = a;
            this.sensorTester = sensorTester;
        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            setContentView(R.layout.save_dialog);
            dataManager = new DataManager(getApplicationContext());
            spinner = findViewById(R.id.spinner);

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getApplicationContext(),
                    android.R.layout.simple_spinner_item, dataManager.getAllExperiments());
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spinner.setAdapter(adapter);

            meritevName = findViewById(R.id.poskusName);
            setButtons();
        }

        void setButtons(){
            cancelButton = findViewById(R.id.cancel_button);
            recordButton = findViewById(R.id.record_button);

            cancelButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                }
            });

            recordButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String poskus = spinner.getSelectedItem().toString();
                    String name = meritevName.getText().toString();
                    sensorTester.startTime = System.currentTimeMillis();
                    sensorTester.dataLogger = new DataLogger(getApplicationContext(), poskus, name);
                    sensorTester.fab.setImageResource(R.drawable.rectbackgroundblack);
                    Toast.makeText(getApplicationContext(), "Started recording", Toast.LENGTH_LONG).show();
                    dismiss();

                }
            });
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sensor_tester);
        prefsManager = new PrefsManager(getApplicationContext());
        sampleRate = prefsManager.getSampleRate();

        calculatorPPM = new CalculatorPPM(getApplicationContext());

        timeFormat = new SimpleDateFormat("HH:mm:ss");
        timeFormat.setTimeZone(tz);

        timeText = findViewById(R.id.timeText);

        setGraphs();

        textDisplays = new TextView[]{
                findViewById(R.id.mq135_display),
                findViewById(R.id.mq3_display),
                findViewById(R.id.temp_display),
                findViewById(R.id.hum_display),
                findViewById(R.id.mq136_display),
                findViewById(R.id.mq138_display)
        };




        /*mq3Graph.mChart.getAxisLeft().setAxisMaximum(0.45f);
        mq3Graph.mChart.getAxisLeft().setAxisMinimum(0.39f);
        tempGraph.mChart.getAxisLeft().setAxisMaximum(100);
        humGraph.mChart.getAxisLeft().setAxisMaximum(100);*/

        for(int i = 0; i < graphs.length; i++){
            graphs[i].mChart.setBackgroundColor(Color.parseColor("#454545"));
            graphs[i].mChart.getAxisLeft().setTextColor(Color.parseColor("#ffffff"));
            graphs[i].mChart.getLegend().setEnabled(false);
        }


        final SaveDialog saveDialog = new SaveDialog(this, this);

        fab = findViewById(R.id.floatingActionButton);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(dataLogger != null){
                    Toast.makeText(getApplicationContext(), "Recording saved", Toast.LENGTH_LONG).show();
                    timeText.setText("");
                    dataLogger.saveData();
                    dataLogger = null;
                    fab.setImageResource(R.drawable.cerclebackgroundblack);
                }else{
                    saveDialog.show();
                }
            }
        });

        GlobalManager.bluetoothSerial.setMessageHandler(new BluetoothSerial.MessageHandler() {
            @SuppressLint("DefaultLocale")
            @Override
            public void onReceive(String text) {
                try {
                    String[] sensorData = text.split("\\s+");
                    // calculate ppm values
                    calculatorPPM.calculate_MQ135_ppm(Float.parseFloat(sensorData[0]));
                    calculatorPPM.calculate_MQ3_ppm(Float.parseFloat(sensorData[1]));



                    textDisplays[0].setText(String.format("%s PPM", df.format(calculatorPPM.MQ3_last_ppm)));
                    textDisplays[1].setText(String.format("%s PPM", df.format(calculatorPPM.MQ135_last_ppm)));
                    textDisplays[2].setText(String.format("%s°C", sensorData[2]));
                    textDisplays[3].setText(String.format("%s%%", sensorData[3]));
                    textDisplays[4].setText(String.format("%s PPM", sensorData[4]));
                    textDisplays[5].setText(String.format("%s PPM", sensorData[5]));


                    graphs[0].addEntry(calculatorPPM.MQ135_last_ppm);
                    graphs[1].addEntry(calculatorPPM.MQ3_last_ppm);
                    graphs[2].addEntry(Float.parseFloat(sensorData[2]));
                    graphs[3].addEntry(Float.parseFloat(sensorData[3]));
                    graphs[4].addEntry(Float.parseFloat(sensorData[4]));
                    graphs[5].addEntry(Float.parseFloat(sensorData[5]));

                    if(dataLogger != null){
                        int millis = (int)System.currentTimeMillis() - (int)startTime;
                        String formatedText = String.format("%02d:%02d:%02d",
                                TimeUnit.MILLISECONDS.toHours(millis),
                                TimeUnit.MILLISECONDS.toMinutes(millis) -
                                        TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis)), // The change is in this line
                                TimeUnit.MILLISECONDS.toSeconds(millis) -
                                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis)));
                        timeText.setText(formatedText);
                        dataLogger.insertData(
                                // Time
                                String.valueOf((System.currentTimeMillis() - startTime) / 1000f),
                                // MQ-135
                                sensorData[0],
                                // MQ-3
                                sensorData[1],
                                // temp
                                sensorData[2],
                                // hum
                                sensorData[3],
                                // MQ-136
                                sensorData[4],
                                // MQ-138
                                sensorData[5]);
                        }

                }catch (Exception e){
                    Log.e("Bluetooth receive", e.toString());
                }
            }
        });



        valueUpdater = new SensorValueUpdater();
        valueUpdater.start();
    }


    void setGraphs(){
        graphs = new SensorGraphCard[]{
                new SensorGraphCard((LineChart)findViewById(R.id.mq135Graph)),
                new SensorGraphCard((LineChart)findViewById(R.id.mq3Graph)),
                new SensorGraphCard((LineChart)findViewById(R.id.tempGraph)),
                new SensorGraphCard((LineChart)findViewById(R.id.humGraph)),
                new SensorGraphCard((LineChart)findViewById(R.id.mq136Graph)),
                new SensorGraphCard((LineChart)findViewById(R.id.mq138Graph))
        };

    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        valueUpdater.stopThread();
    }

    private class SensorValueUpdater extends Thread {
        private boolean running = true;
        @Override
        public void run() {
            while (running) {
                GlobalManager.bluetoothSerial.write("63");
                try {
                    Thread.sleep(sampleRate);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        public void stopThread(){
            running = false;
        }
    }
}
