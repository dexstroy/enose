package com.example.myfirstapp.dataLog;

import android.content.Context;

import com.example.myfirstapp.CustomToast;

import java.io.File;

public class DataManager {
    private Context context;
    private File experimentFolder;

    public DataManager(Context context){
        this.context = context;
        File mainDir = this.context.getFilesDir();

        experimentFolder = new File(mainDir, "experiments");
        experimentFolder.mkdir();
    }

    public boolean createExperiment(String name){
        File experiment = new File(experimentFolder, name);
        boolean created = experiment.mkdir();


        CustomToast.toast(created?"Created":"Already exists", context);

        return created;
    }

    public File getExperimentMeasurement(String experimenName, String measurementName){
        File experiment = new File(experimentFolder, experimenName);
        File measurement = new File(experiment, measurementName);

        return measurement;
    }

    public String[] getAllExperiments(){
        return experimentFolder.list();
    }

    public String[] getAllMeasurements(String experiment){
        File measurementsFolder = new File(experimentFolder, experiment);
        return measurementsFolder.list();
    }

}
