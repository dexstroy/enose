package com.example.myfirstapp.dataLog;

import android.content.Context;
import android.util.Log;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class DataLogger {
    private Context context;
    private File data;
    private FileWriter writer;
    private File experimentFolder;

    public DataLogger(Context context, String experimentName, String measureName){
        this.context = context;
        File mainDir = this.context.getFilesDir();

        experimentFolder = new File(mainDir, "experiments");
        experimentFolder.mkdir();

        File experiment = new File(experimentFolder, experimentName);
        experiment.mkdir();

        File data = new File(experiment, measureName + ".csv");

        try {
            writer = new FileWriter(data);
            writer.append("time, mq135, mq3, hum, temp\n");
        } catch (IOException e) {
            Log.d("Tuki", "Error");
            e.printStackTrace();
        }
    }

    // Measurements are saved with names data_1, data_2, this functions returns biggest number
    private int getNextNumber(File[] files){
        int maxNumber = -1;

        for(int i = 0; i < files.length; i++){
            try {
                int number = Integer.parseInt(files[i].getName().split("_")[1].split("\\.")[0]);
                if(number > maxNumber){
                    maxNumber = number;
                }
            }catch (Exception e){
                Log.e("Error", e.toString());
            }
        }

        return maxNumber + 1;
    }

    // returns true if data inserted
    public boolean insertData(String time, String mq135, String mq3, String temp, String hum, String mq136, String mq138){
        if(writer == null) return false;

        try {
            writer.append(time).append(",").append(mq135).append(",").append(mq3).append(",").append(temp).append(",").append(hum).append(mq136).append(mq138).append("\n");
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }


    // saves data and returns true if successful
    public boolean saveData(){
        try {
            writer.flush();
            writer.close();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }


}
