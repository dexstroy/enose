package com.example.myfirstapp.graph;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.example.myfirstapp.BluetoothSerial;
import com.example.myfirstapp.GlobalManager;
import com.example.myfirstapp.R;
import com.example.myfirstapp.dataLog.DataManager;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;


import com.github.mikephil.charting.utils.ColorTemplate;
import com.opencsv.CSVReader;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class GraphView extends AppCompatActivity  {

    private LineChart chart;
    private Thread thread;
    private boolean plotData = true;
    DataManager dm;

    String experimentName;
    String[] selectedMeasurements;
    int selectedSensor;


    public static final int[] colors = {
            Color.rgb(193, 37, 82),
            Color.rgb(255, 102, 0),
            Color.rgb(245, 199, 0),
            Color.rgb(106, 150, 31),
            Color.rgb(179, 100, 53)
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_graph_view);
        dm = new DataManager(getApplicationContext());

        chart = (LineChart) findViewById(R.id.chart2);

        // enable description text
        chart.getDescription().setEnabled(true);

        // enable touch gestures
        chart.setTouchEnabled(true);

        // enable scaling and dragging
        chart.setDragEnabled(true);
        chart.setScaleEnabled(true);
        chart.setDrawGridBackground(false);

        // if disabled, scaling can be done on x- and y-axis separately
        chart.setPinchZoom(true);

        // set an alternative background color
        chart.setBackgroundColor(Color.WHITE);

        LineData data = new LineData();
        data.setValueTextColor(Color.WHITE);

        // add empty data
        chart.setData(data);

        // get the legend (only possible after setting data)
        /*Legend l = chart.getLegend();

        // modify the legend ...
        l.setForm(Legend.LegendForm.LINE);
        l.setTextColor(Color.WHITE);*/

        XAxis xl = chart.getXAxis();
        xl.setTextColor(Color.WHITE);
        xl.setDrawGridLines(true);
        xl.setAvoidFirstLastClipping(true);
        xl.setEnabled(true);

        YAxis leftAxis = chart.getAxisLeft();
        leftAxis.setTextColor(Color.WHITE);
        leftAxis.setDrawGridLines(false);
        leftAxis.setAxisMaximum(600);
        leftAxis.setAxisMinimum(0);
        leftAxis.setDrawGridLines(true);


        XAxis xAxis = chart.getXAxis();
        xAxis.setAxisMaximum(1800);
        xAxis.setAxisMinimum(0f);

        //chart.setVisibleXRange(0, 1800);
        chart.setAutoScaleMinMaxEnabled(true);


        YAxis rightAxis = chart.getAxisRight();
        rightAxis.setEnabled(false);

        chart.getAxisLeft().setDrawGridLines(false);
        chart.getXAxis().setDrawGridLines(false);
        chart.setDrawBorders(false);

        chart.animateX(1500);

        chart.setBackgroundColor(Color.parseColor("#454545"));
        chart.getAxisLeft().setTextColor(Color.parseColor("#ffffff"));
        chart.getLegend().setEnabled(true);

        chart.setDragEnabled(true);
        chart.setScaleEnabled(true);
        // chart.setScaleXEnabled(true);
        // chart.setScaleYEnabled(true);

        // force pinch zoom along both axis
        chart.setPinchZoom(true);

        feedMultiple();

        Legend l = chart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.BOTTOM);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.LEFT);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(false);


        csv_read();



        /*for(int i = 0; i < 200; i++){
            addEntry((int)(Math.random() * 10));
        }*/


    }

    void csv_read(){
        experimentName = getIntent().getStringExtra("experimentName");
        selectedMeasurements = getIntent().getStringArrayExtra("measurements");
        selectedSensor = getIntent().getIntExtra("selectedSensor", 1);


        chart.resetTracking();
        ArrayList<ILineDataSet> dataSets = new ArrayList<>();

        FileReader fileReader;
        try {
            for(int i = 0; i < selectedMeasurements.length; i++){
                fileReader = new FileReader(dm.getExperimentMeasurement(experimentName, selectedMeasurements[i]));

                CSVReader csvReader = new CSVReader(fileReader);
                List<String[]> allData = csvReader.readAll();

                ArrayList<Entry> values = new ArrayList<>();


                for(int j = 1; j < allData.size(); j++){
                    if(j % 10 != 0) continue;
                    if(j < 10)
                        values.add(new Entry(j, Float.parseFloat(allData.get(j)[selectedSensor])));
                    else{
                        values.add(new Entry(j,
                                (
                                    Float.parseFloat(allData.get(j)[selectedSensor]) +
                                    Float.parseFloat(allData.get(j - 1)[selectedSensor]) +
                                    Float.parseFloat(allData.get(j - 2)[selectedSensor]) +
                                    Float.parseFloat(allData.get(j - 3)[selectedSensor]) +
                                    Float.parseFloat(allData.get(j - 4)[selectedSensor]) +
                                    Float.parseFloat(allData.get(j - 5)[selectedSensor]) +
                                    Float.parseFloat(allData.get(j - 6)[selectedSensor]) +
                                    Float.parseFloat(allData.get(j - 7)[selectedSensor]) +
                                    Float.parseFloat(allData.get(j - 8)[selectedSensor]) +
                                    Float.parseFloat(allData.get(j - 9)[selectedSensor])
                                ) / 10
                        ));
                    }
                }

                LineDataSet d = new LineDataSet(values, selectedMeasurements[i].replace(".csv", ""));
                d.setLineWidth(2.5f);
                d.setDrawCircles(false);

                d.setColor(colors[i % colors.length]);
                dataSets.add(d);
            }

            LineData data = new LineData(dataSets);
            chart.setData(data);
            chart.invalidate();


            Log.d("CSV", "read");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return;
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }


    }



    private void addEntry(int mq3Data) {

        LineData data = chart.getData();

        if (data != null) {

            ILineDataSet set = data.getDataSetByIndex(0);
            // set.addEntry(...); // can be called as well

            if (set == null) {
                set = createSet();
                data.addDataSet(set);
            }

            //data.addEntry(new Entry(set.getEntryCount(), (float) (Math.random() * 80) + 10f), 0);
            data.addEntry(new Entry(set.getEntryCount(), (float)mq3Data), 0);
            //data.addEntry(new Entry(set.getEntryCount(), event.values[0] + 5), 0);
            data.notifyDataChanged();

            // let the chart know it's data has changed
            chart.notifyDataSetChanged();

            // limit the number of visible entries
            chart.setVisibleXRangeMaximum(1800);
            // chart.setVisibleYRange(30, AxisDependency.LEFT);

            // move to the latest entry
            chart.moveViewToX(data.getEntryCount());

        }
    }

    private LineDataSet createSet() {

        LineDataSet set = new LineDataSet(null, "Dynamic Data");
        set.setAxisDependency(YAxis.AxisDependency.LEFT);
        set.setLineWidth(1f);
        set.setColor(Color.MAGENTA);
        set.setHighlightEnabled(false);
        set.setDrawValues(false);
        set.setDrawCircles(false);
        set.setMode(LineDataSet.Mode.CUBIC_BEZIER);
        set.setCubicIntensity(0.2f);
        return set;
    }

    private void feedMultiple() {

        if (thread != null){
            thread.interrupt();
        }

        thread = new Thread(new Runnable() {

            @Override
            public void run() {
                while (true){
                    plotData = true;
                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
        });

        thread.start();
    }
}
