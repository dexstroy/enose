package com.example.myfirstapp;

import android.content.Context;
import android.widget.Toast;

public class CustomToast {
    public static void toast(String message, Context context){
        int duration = Toast.LENGTH_SHORT;

        Toast toast = Toast.makeText(context, message, duration);
        toast.show();
    }
}
