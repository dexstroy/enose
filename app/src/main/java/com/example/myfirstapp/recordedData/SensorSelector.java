package com.example.myfirstapp.recordedData;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.myfirstapp.R;
import com.example.myfirstapp.graph.GraphView;

import java.util.Arrays;

public class SensorSelector extends AppCompatActivity {

    ListView listView;

    String[] selectedMeasurements;
    String experimentName;

    String[] sensorNames = {"MQ-135", "MQ-3", "Temperature", "Humidity", "MQ-136", "MQ-138"};

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sensor_selector);

        setTitle(R.string.selectSensorTitle);

        experimentName = getIntent().getStringExtra("experimentName");
        selectedMeasurements = getIntent().getStringArrayExtra("measurements");
        Log.d("ReceivedArray", Arrays.toString(selectedMeasurements));

        listView = findViewById(R.id.sensor_listview);
        updateListView();

    }

    void updateListView(){
        ArrayAdapter adapter = new ArrayAdapter<String>(this,R.layout.list_view_tile, sensorNames);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d("Item click", sensorNames[position]);
                Intent intent = new Intent(SensorSelector.this, GraphView.class);
                intent.putExtra("measurements", selectedMeasurements);
                intent.putExtra("experimentName", experimentName);
                intent.putExtra("selectedSensor", position + 1);
                startActivity(intent);
            }
        });
    }
}
