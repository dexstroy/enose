package com.example.myfirstapp.recordedData;



import android.app.ActionBar;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.example.myfirstapp.CustomToast;
import com.example.myfirstapp.R;
import com.example.myfirstapp.dataLog.DataManager;


public class PoskusBrowser extends AppCompatActivity {
    NewExperimentDialog newExperimentDialog;
    ListView listView;
    DataManager dm;
    String[] allExperiments;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.poskus_browser);
        newExperimentDialog = new NewExperimentDialog(this, this);
        dm = new DataManager(getApplicationContext());

        listView = findViewById(R.id.experiment_listview);
        setTitle(R.string.experimentsTitle);
        updateListView();
        final FloatingActionButton fab = findViewById(R.id.floatBTN);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("button", "Pressed");
                newExperimentDialog.show();
            }
        });
    }

    void updateListView(){
        allExperiments = dm.getAllExperiments();
        ArrayAdapter adapter = new ArrayAdapter<String>(this,R.layout.list_view_tile, allExperiments);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d("Item click", allExperiments[position]);
                Intent intent = new Intent(PoskusBrowser.this, DanSelector.class);
                intent.putExtra("experimentName", allExperiments[position]);
                startActivity(intent);

            }
        });
    }

    class NewExperimentDialog extends Dialog {

        public Activity c;
        Button cancelButton, createButton;
        EditText poskusName;
        PoskusBrowser pb;

        @Override
        public <T extends View> T findViewById(int id) {
            return super.findViewById(id);
        }

        public NewExperimentDialog(Activity a, PoskusBrowser pb) {
            super(a);
            // TODO Auto-generated constructor stub
            this.c = a;
            this.pb = pb;
        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            setContentView(R.layout.new_experiment_dialog);
            poskusName = findViewById(R.id.poskusName);
            buttonSetup();
        }

        void buttonSetup(){
            cancelButton = findViewById(R.id.cancel_button);
            createButton = findViewById(R.id.create_button);

            cancelButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                }
            });

            createButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    DataManager dataManager = new DataManager(getApplicationContext());
                    if(poskusName.getText().toString().equals("")){
                        CustomToast.toast("Please enter experiment name", getApplicationContext());
                        return;
                    }
                    dataManager.createExperiment(poskusName.getText().toString());
                    pb.updateListView();
                    dismiss();
                }
            });
        }
    }
}

