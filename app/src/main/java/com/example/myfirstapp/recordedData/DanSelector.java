package com.example.myfirstapp.recordedData;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ListView;
import android.widget.TextView;

import com.example.myfirstapp.R;
import com.example.myfirstapp.SensorTester;
import com.example.myfirstapp.dataLog.DataManager;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DanSelector extends AppCompatActivity {
    ListView listView;
    DataManager dm;
    String[] allMeasurements;
    String experimentName;
    FloatingActionButton fab;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dan_selector);

        setTitle(R.string.measurementsTitle);

        experimentName = getIntent().getStringExtra("experimentName");
        dm = new DataManager(getApplicationContext());
        listView = findViewById(R.id.dan_listview);

        updateListView();

        fab = findViewById(R.id.floatBTNdan);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DanSelector.this, SensorSelector.class);
                intent.putExtra("measurements", getSelectedExperiments());
                intent.putExtra("experimentName", experimentName);
                startActivity(intent);
            }
        });

        //updateFloatingButton();
        fab.hide();
    }

    String[] getSelectedExperiments(){
        ArrayList<String> selectedMeasurements = new ArrayList<>();
        View v;
        CheckBox c;
        for(int i = 0; i < listView.getCount(); i++){
            v = listView.getChildAt(i);
            c = v.findViewById(R.id.list_view_checkbox);

            if(c.isChecked()){
                selectedMeasurements.add(allMeasurements[i]);
            }
        }
        return selectedMeasurements.toArray(new String[0]);
    }

    void updateListView(){
        allMeasurements = dm.getAllMeasurements(experimentName);
        //ArrayAdapter adapter = new ArrayAdapter<String>(this,R.layout.list_view_tile_check_box, allMeasurements);
        CheckBoxArrayAdapter adapter = new CheckBoxArrayAdapter(this, allMeasurements);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d("Item click", String.valueOf(position));
                /*Intent intent = new Intent(PoskusBrowser.this, DanSelector.class);
                intent.putExtra("experimentName", allExperiments[position]);
                startActivity(intent);*/

            }
        });
    }

    void updateFloatingButton(){
        View v;
        CheckBox c;
        for(int i = 0; i < listView.getCount(); i++){
            v = listView.getChildAt(i);
            c = v.findViewById(R.id.list_view_checkbox);

            if(c.isChecked()){
                fab.show();
                return;
            }
        }
        fab.hide();
    }

    class CheckBoxArrayAdapter extends ArrayAdapter<String> {
        private final Context context;
        private final String[] values;
        CheckBox checkBox;

        public CheckBoxArrayAdapter(Context context, String[] values) {
            super(context, -1, values);
            this.context = context;
            this.values = values;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View rowView = inflater.inflate(R.layout.list_view_tile_check_box, parent, false);

            TextView textView = rowView.findViewById(R.id.firstLine);
            textView.setText(values[position]);

            checkBox = rowView.findViewById(R.id.list_view_checkbox);
            checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    Log.d("Checkbox", String.valueOf(position));
                    updateFloatingButton();
                }
            });

            return rowView;
        }
    }
}



