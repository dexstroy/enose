package com.example.myfirstapp.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseManager extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "database.db";
    public static final String TABLE_NAME = "measure_table";

    public static final String COL_1 = "ID";
    public static final String COL_2 = "MQ135";
    public static final String COL_3 = "MQ3";

    public DatabaseManager(Context context, String name, SQLiteDatabase.CursorFactory factory, int version){
        super(context, name, factory, version);
    }

    public DatabaseManager(Context context){
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    // if database file was not existing onCreate is called
    public void onCreate(SQLiteDatabase db) {
        Log.d("EVENT", "ON CREATE CALL");
        db.execSQL("create table " + TABLE_NAME + "(ID INTEGER PRIMARY KEY AUTOINCREMENT, MQ135 INTEGER, MQ3 INTEGER)");
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
    }

    public boolean insertData(int MQ135Value,int MQ3Value){
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues contentValues = new ContentValues();
        contentValues.put(COL_2, MQ135Value);
        contentValues.put(COL_3, MQ3Value);

        long result = db.insertOrThrow(TABLE_NAME, null, contentValues);


        // check if data was inserted in database
        return result != -1;
    }

}

