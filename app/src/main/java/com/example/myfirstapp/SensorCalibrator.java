package com.example.myfirstapp;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.myfirstapp.sharedPrefs.PrefsManager;


public class SensorCalibrator extends AppCompatActivity {
    boolean calibrateMQ135 = false;
    boolean calibrateMQ3 = false;

    int num_of_measures = 30;
    int MQ135_measures = 0;
    int MQ3_measures = 0;

    int[] MQ135_values;
    int[] MQ3_values;

    Button mq135_calibrate_button, mq3_calibrate_button;

    SensorValueUpdater valueUpdater;

    private ProgressDialog progress;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sensor_calibrator);

        MQ135_values = new int[num_of_measures];
        MQ3_values = new int[num_of_measures];

        valueUpdater = new SensorValueUpdater();

        setBluetoothReceive();
        buttonSetup();
    }

    public void buttonSetup(){

        mq135_calibrate_button = findViewById(R.id.mq135_calibrate);
        mq3_calibrate_button = findViewById(R.id.mq3_calibrate);


        mq135_calibrate_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calibrateMQ135 = true;
                if(!valueUpdater.isAlive()) valueUpdater.start();
                if(progress == null){
                    progress = ProgressDialog.show(SensorCalibrator.this, "Calibrating...", "Please Wait!!!");
                }
            }
        });

        mq3_calibrate_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calibrateMQ3 = true;
                if(!valueUpdater.isAlive()) valueUpdater.start();
                if(progress == null){
                    progress = ProgressDialog.show(SensorCalibrator.this, "Calibrating...", "Please Wait!!!");
                }
            }
        });
    }

    public void setBluetoothReceive(){
        GlobalManager.bluetoothSerial.setMessageHandler(new BluetoothSerial.MessageHandler() {
            @Override
            public void onReceive(String text) {
                try {
                    Log.d("Received", text);
                    String[] sensorData = text.split("\\s+");


                    if(calibrateMQ135){
                        MQ135_values[MQ135_measures] = Integer.parseInt(sensorData[0]);
                        MQ135_measures += 1;
                        if(MQ135_measures >= num_of_measures){
                            calibrateMQ135 = false;
                            PrefsManager prefsManager = new PrefsManager(getApplicationContext());
                            prefsManager.saveMQ135_R0_value(getAverage(MQ135_values));
                            MQ135_measures = 0;
                            MQ135_values = new int[num_of_measures];
                        }
                    }

                    if(calibrateMQ3){
                        MQ3_values[MQ3_measures] = Integer.parseInt(sensorData[1]);
                        MQ3_measures += 1;
                        if(MQ3_measures >= num_of_measures){
                            calibrateMQ3 = false;
                            PrefsManager prefsManager = new PrefsManager(getApplicationContext());
                            prefsManager.saveMQ3_R0_value(getAverage(MQ3_values));
                            MQ3_measures = 0;
                            MQ3_values = new int[num_of_measures];
                        }
                    }
                }catch (Exception e){
                    Log.e("Bluetooth receive", e.toString());
                }
            }
        });
    }

    private int getAverage(int[] lista){
        int sum = 0;
        for(int i = 0; i < lista.length; i++){
            sum += lista[i];
        }
        return  sum / lista.length;
    }

    private class SensorValueUpdater extends Thread {
        private boolean running = true;
        @Override
        public void run() {
            while (running) {
                if(calibrateMQ3 || calibrateMQ135){
                    GlobalManager.bluetoothSerial.write("63");
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }else{
                    if(progress != null){
                        progress.dismiss();
                        progress = null;
                    }
                }

            }
        }

        public void stopThread(){
            running = false;
        }
    }
}
