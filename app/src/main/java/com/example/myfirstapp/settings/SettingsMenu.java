package com.example.myfirstapp.settings;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;

import com.example.myfirstapp.R;
import com.example.myfirstapp.sharedPrefs.PrefsManager;

public class SettingsMenu extends AppCompatActivity {
    EditText sampleRateText;
    PrefsManager prefsManager;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings_menu);

        setTitle(R.string.settingsTitle);
        prefsManager = new PrefsManager(getApplicationContext());

        sampleRateText = findViewById(R.id.sampleRateText);
        sampleRateText.setText(String.valueOf(prefsManager.getSampleRate()));
    }

    @Override
    public void onBackPressed() {
        if(!sampleRateText.getText().toString().equals("")){
            prefsManager.saveSampleRate(Integer.parseInt(sampleRateText.getText().toString()));
        }

        super.onBackPressed();
    }
}

