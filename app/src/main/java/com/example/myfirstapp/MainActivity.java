package com.example.myfirstapp;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Set;

public class MainActivity extends AppCompatActivity {

    BluetoothAdapter mBluetoothAdapter;
    BluetoothSerial bluetoothSerial;

    // Buttons
    Button enable_button, search_button, thread_button;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonsSetup();
    }

    private void buttonsSetup(){
        search_button = findViewById(R.id.search_button);
        thread_button = findViewById(R.id.thread_button);

        enable_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                enableBluetooth();
            }
        });

        search_button.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                getPairedDevices();
            }
        });

        thread_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                connect();
            }
        });

    }

    private void enableBluetooth(){
        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if(mBluetoothAdapter != null){
            if(!mBluetoothAdapter.isEnabled()){
                mBluetoothAdapter.enable();
                toast("Bluetooth enabled");
            }else{
                toast("Bluetooth already enabled");
            }
        }else{
            toast("No bluetooth");
        }
    }

    private void toast(String message){
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }

    private void getPairedDevices(){
        Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
        ArrayList list = new ArrayList();

        if(pairedDevices.size() > 0){
            for(BluetoothDevice device : pairedDevices){
                list.add(device.getName() + "\n" + device.getAddress());
                Log.d("Device name", device.getName());
            }
        }

        //final ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, list);
    }

    public void connect(){
        if(bluetoothSerial != null)
            return;
    }

    public String charArrayToString(byte[] buffer){
        StringBuilder s = new StringBuilder();

        for(int i = 0; i < buffer.length; i++){
            s.append(buffer[i]).append("|");
        }
        return s.toString();
    }
}

