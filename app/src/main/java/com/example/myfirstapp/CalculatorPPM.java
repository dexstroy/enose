package com.example.myfirstapp;

import android.content.Context;
import android.util.Log;

import com.example.myfirstapp.sharedPrefs.PrefsManager;

public class CalculatorPPM {
    private float MQ3_R0;
    private float MQ135_R0;

    public float MQ3_last_ppm = 0;
    public float MQ135_last_ppm = 0;


    public CalculatorPPM(Context context){
        PrefsManager prefsManager = new PrefsManager(context);
        MQ3_R0 = (float)prefsManager.getMQ3_R0_value();
        MQ135_R0 = (float)prefsManager.getMQ135_R0_value();

        Log.d("MQ3_R0", String.valueOf(MQ3_R0));
        Log.d("MQ135_R0", String.valueOf(MQ135_R0));

        // Calculate resistance
        MQ3_R0 = (1023 - MQ3_R0) / 1023;
        MQ135_R0 = (1023 - MQ135_R0) / 1023;
    }


    public void calculate_MQ3_ppm(float RS){
        RS = (1023 - RS) / 1023;
        MQ3_last_ppm = 0.401896f * (float)Math.pow(RS / MQ3_R0, -1.511867f);
    }

    public void calculate_MQ135_ppm(float RS){
        RS = (1023 - RS) / 1023;
        MQ135_last_ppm = 118.831209f * (float)Math.pow(RS / MQ135_R0, -2.569887f);
    }
}
