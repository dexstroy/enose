package com.example.myfirstapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myfirstapp.recordedData.PoskusBrowser;
import com.example.myfirstapp.settings.SettingsMenu;


public class MainMenu extends AppCompatActivity {

    CardView testSensorsCard, calibrateSensorsCard, recordedDataCard, settingsMenuCard, bluetooothCard;

    CalculatorPPM calculatorPPM;

    SeekBar motorSpeed;

    TextView connectionStatus, connectionMessage, fanSpeedText;
    ImageView bluetoothStatusImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);

        setTitle(R.string.mainMenuTitle);

        motorSpeed = findViewById(R.id.motorSpeed);
        connectionStatus = findViewById(R.id.connectionStatus);
        connectionMessage = findViewById(R.id.connectionMessage);
        bluetoothStatusImage = findViewById(R.id.bluetoothStatusImage);
        fanSpeedText = findViewById(R.id.fanSpeedText);
        fanSpeedText.setText("Fan speed: 0%");


        motorSpeed.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                fanSpeedText.setText(String.format("Fan speed: %s%%", String.valueOf( (int)((float)progress / (float)255 * 100))) );
                if(GlobalManager.bluetoothSerial != null){
                    GlobalManager.bluetoothSerial.write("M" + String.valueOf(progress));
                    Log.d("Motor", String.valueOf(progress));
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        calculatorPPM = new CalculatorPPM(getApplicationContext());


        cardsSetup();
        connect();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(GlobalManager.bluetoothSerial != null){
            GlobalManager.bluetoothSerial.setMessageHandler(messageHandler());
        }
    }

    @Override
    public void onBackPressed() {
        if(GlobalManager.bluetoothSerial != null){
            GlobalManager.bluetoothSerial.close();
            GlobalManager.bluetoothSerial = null;
        }

        super.onBackPressed();
    }

    private void cardsSetup(){
        testSensorsCard = findViewById(R.id.test_sensors);
        calibrateSensorsCard = findViewById(R.id.calibrate_sensors);
        recordedDataCard = findViewById(R.id.recorded_data);
        settingsMenuCard = findViewById(R.id.settings_button);
        bluetooothCard = findViewById(R.id.bluetoothCard);


        testSensorsCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(GlobalManager.bluetoothSerial != null && GlobalManager.bluetoothSerial.isConnected()){
                    Intent i = new Intent(MainMenu.this, SensorTester.class);
                    startActivity(i);
                }else{
                    toast("To record data you must be connected to device");
                }

            }
        });

        calibrateSensorsCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(GlobalManager.bluetoothSerial != null && GlobalManager.bluetoothSerial.isConnected()){
                    Intent i = new Intent(MainMenu.this, SensorCalibrator.class);
                    startActivity(i);
                }else{
                    toast("To calibrate sensors you must be connected to device");
                }
            }
        });

        recordedDataCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainMenu.this, PoskusBrowser.class);
                startActivity(i);
            }
        });

        settingsMenuCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainMenu.this, SettingsMenu.class);
                startActivity(i);
            }
        });

        bluetooothCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(GlobalManager.bluetoothSerial == null){
                    Intent i = new Intent(MainMenu.this, DeviceList.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_TASK_ON_HOME);

                    startActivity(i);
                }else{
                    toast("You are connected");
                }
            }
        });
    }

    private void toast(String message){
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }


    public void connect(){

        Intent newint = getIntent();
        String address = newint.getStringExtra(DeviceList.EXTRA_ADDRESS);

        if(GlobalManager.bluetoothSerial == null){
            connectionStatus.setText("Status: Not connected");
            connectionMessage.setText("Click here to connect");
            bluetoothStatusImage.setImageResource(R.drawable.ic_bluetooth_disabled_black_24dp);
            bluetoothStatusImage.setBackgroundResource(R.drawable.cerclebackgroundgrey);
            return;
        }


        GlobalManager.bluetoothSerial = new BluetoothSerial(this, messageHandler(), address);

        GlobalManager.bluetoothSerial.connect();
        connectionStatus.setText("Status: Connected");
        connectionMessage.setText("");
        bluetoothStatusImage.setImageResource(R.drawable.ic_bluetooth_black_24dp);
        bluetoothStatusImage.setBackgroundResource(R.drawable.cerclebackgroundgreen);
    }

    public BluetoothSerial.MessageHandler messageHandler(){
        return  new BluetoothSerial.MessageHandler() {
            @Override
            public void onReceive(String text) {
                Log.d("Received main menu", text);
            }
        };
    }

    public String charArrayToString(byte[] buffer){
        StringBuilder s = new StringBuilder();

        for(int i = 0; i < buffer.length; i++){
            s.append(buffer[i]).append("|");
        }
        return s.toString();
    }
}
