# Android Application for Capturing Data from Bluetooth Device

## About
This Android application serves as an alternative to the desktop application in the project [System for Measuring Fruit Ripening Stages](https://gitlab.com/dexstroy/enose_v2). The measuring device communicates with an Android phone over Bluetooth. The main goal of this project was to make the [System for Measuring Fruit Ripening Stages](https://gitlab.com/dexstroy/enose_v2) more portable for taking measurements outside the office.

## Mobile Application Functionalities
Before using the application, we need to connect to our Bluetooth device:

![Local Image](images/blueSelect.jpg)

After connecting, we are redirected to the main menu:

![Local Image](images/app_main_menu.png)

Before we start measuring data, we need to perform sensor calibration to find their stable values:

![Local Image](images/Calibrate_menu.jpg)

We can view live data from sensors updated every second:

![Local Image](images/SensorTester.JPG)

We can also record data and save it to a specific experiment:

![Local Image](images/recordDialog.png)

After capturing data, we can display and compare it:

![Local Image](images/graphView.png)
